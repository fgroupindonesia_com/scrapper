//
// (C) FGroupIndonesia.com team.
// proudly present the _zipper.js used as a part of scrapper project
// ===========================================
// first-release-date: 14/sept/2020
// category: internet-marketing, data-scrapper
// ===========================================
// license: Apache License 2.0 and BSD.
// ===========================================
// feel free to make 'pull request' for enhancing the features available from this nodejs app.
// 

var zlib = require('zlib');
var fs = require('fs');

const mainFolder = './node_modules/puppeteer/.local-chromium/win64-756035/chrome-win/';

const target1 = 'chrome.dll';
const target2 = 'interactive_ui_tests.exe';
const zipped1 = 'chrome.gz';
const zipped2 = 'interactive_ui_tests.gz';

var myArgs = process.argv.slice(2);

// valid args :
// node _zipper.js -extract
// or
// node _zipper.js -compress
var mode = myArgs[0];

if(myArgs.length>0){

		if(mode.includes('extract')){
		
			console.log('extracting...');
			extracting();
		}else {
			
			console.log('compressing...');
			compressing();
		}
		
		console.log('process done!');
		
}else{

console.log('your command is not properly good!');

}

async function extracting(){
	
			await extractZip(mainFolder+zipped1);
			await extractZip(mainFolder+zipped2);					
			await deleteFile(mainFolder+zipped1);
			await deleteFile(mainFolder+zipped2);
	
}

async function compressing(){
	
			await makeZip(mainFolder+target1);
			await makeZip(mainFolder+target2);
			await deleteFile(mainFolder+target1);
			await deleteFile(mainFolder+target2);
}

async function deleteFile(fileName){
	
try {
  fs.unlinkSync(fileName);
  
} catch(err) {
  console.error(err);
}
	
}

async function makeZip(fileName){

var gzip = zlib.createGzip();	
var r = fs.createReadStream(fileName);

var newName = fileName.substring(0, fileName.lastIndexOf('.')) || fileName

var newZipName = newName + '.gz'; 

var w = fs.createWriteStream(newZipName);
r.pipe(gzip).pipe(w).on('finish', (err) => {
      if (err) {
		  return reject(err);
	  }
      else {
		return true;  
	  }
    });

}


async function extractZip(fileName){

var gzip = zlib.createGunzip();	
var r = fs.createReadStream(fileName);

var newExtractedName = fileName.substring(0, fileName.lastIndexOf('.')) || fileName

var w = fs.createWriteStream(newExtractedName);

r.pipe(gzip).pipe(w).on('finish', (err) => {
      if (err) {
		  return reject(err);
	  }
      else {
		  
		return true;  
	  }
    });

}

