"use strict";
//
// (C) FGroupIndonesia.com team.
// proudly present the Jualo Data Scrapper v1
// ===========================================
// first-release-date: 14/sept/2020
// category: internet-marketing, data-scrapper
// ===========================================
// license: Apache License 2.0 and BSD.
// ===========================================
// feel free to make 'pull request' for enhancing the features available from this nodejs app.
// 

process.removeAllListeners('warning');

var username = "";
var pass = "";
var urlTarget = "";
var validArguments = false;
var validURL = false;
var stillHasNextPage = false;
var stillVisit = true;
var extractDone = false;


var allURLData = [];
var dataTimerID = [];
var indexURLData = 0;
var successData = 0;
var failedData = 0;
var totalData = 0;

var detailData = [];

var successLogin = false;
var puppeteer;
var browser;
var page;

var jqueryPath = require.resolve('jquery');
var tempFolder = './chromeData';
var databaseFolder = './database/';
var lokasiFinalDB = databaseFolder + tanggalSkrg() + '.db';

var timeWait = 10000; // 10s

// #01: Grabbing the parameter
validArguments = grabArguments();

// #02: Checking the URL 
if(validArguments){
	//console.log(urlTarget);
	console.log('Checking valid jualo url...');
	validURL = isJualoLink();
	if(validURL){
		console.log('Jualo link detected !');
		// new feature added since v1.0.1
		captureCTRLC();
		startProcessing();
	}else{
		console.log('Invalid jualo link!\nProcess halted.');
	}
}

function tanggalSkrg(){
	
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
var time = today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds();

today = yyyy+"_"+ mm + "_"+ dd + "_" + time;
return today;
	
}

async function startProcessing(){
	
	try{
	// #03: Launching Browser, Tab, and the Agent
	await setTheBrowser();
	
	// #04: Filtering the blocking for better performance
	await filterBlocks();
	
	// #05: Visiting the URL and do logging in first
	await visitURL();
	successLogin = await login();
	
	// #06: Extracting the Article URL
	await extractArticleURL();
	
	// #07: Checking does has next page if any
	await isNextPageAvailable();
	
	// #08: Clicking navigation, and grab the main Article URL
	while(stillHasNextPage){
		await clickNextPage();
		await extractArticleURL();
		await isNextPageAvailable();
	}
	
	// #09: Visiting each single articles
	// Extracting the item detail such as ;
	// product name, location, description, and phone picture
	while(stillVisit){
		if(indexURLData == 0 || extractDone == true){
			// this sleep is written for giving a time 
			// capturing the CTRL + C
			await sleep(3000);
			await nextVisitUrl();
			if(stillVisit){
				
			await visitURL();
			await extractDetailArticle();		
			
			}
		}else if(extractDone == false){
			// capturing the CTRL + C also
			await sleep(3000);
		}
	}
	
	await sleep(3000);
	await printout('closing... done!');
	//await page.close();
	await browser.close();
	
	} catch (err){
		
		await printout('some error while waiting in the main process....');
		printout(err.toString());
		printout(err.stack);
	}
	
}

async function nextVisitUrl(){
	try{
	
	
	if(indexURLData == 0 || indexURLData < allURLData.length)
	{
		await printout("now trying to visit another url...");
		
		urlTarget = allURLData[indexURLData].url;
		extractDone = false;
		//await printout('url ' + urlTarget);
	}else{
	stillVisit = false;
	}
	
	} catch(err){
		await printout('some error while nextVisitURL');
		printout(err.toString());
		printout(err.stack);
	}

}

function isJualoLink(){
		
		if(urlTarget.includes('jualo.com/')){
			return true;
		}
		
		return false;
}

function grabArguments(){
	//console.log(process.argv);
	var myArgs = process.argv.slice(2);
	//console.log(myArgs);
	
	// the valid parameter is:
	//node jualo-scrapper.js -u username -p pass -url link
	
	if(myArgs.length==6){
		username = myArgs[1];
		pass = myArgs[3];
		urlTarget = myArgs[5];
		return true;
	}
	
	return false;
	
}

async function setTheBrowser(){
	
	// create the temp folder
	// after deleting the existing
	
	try{

	var fs = require('fs');
	var Path = require('path');
	var alamatTrainned = Path.resolve(__dirname, 'eng.traineddata');
		
		if (fs.existsSync(tempFolder)){
		await deleteDir(tempFolder);
		}
		
		if (fs.existsSync(alamatTrainned)){
			fs.unlinkSync(alamatTrainned);
		}
		
		if (!fs.existsSync(databaseFolder)){
		await fs.mkdirSync(databaseFolder);
		}else{
			// when exist just delete the images
			await deleteFileOnlyInDir(databaseFolder, 'png');
		}
	
	await fs.mkdirSync(tempFolder);
	
	 fs = null;
	 
	var ops = {args:[
		'--disable-background-timer-throttling',
		'--disable-backgrounding-occluded-windows',
		'--disable-renderer-backgrounding',
		'--disable-setuid-sandbox',
        '--disable-accelerated-2d-canvas',
        '--disable-gpu',
		'--disable-dev-shm-usage',
		'--disable-3d-apis',
        '--user-data-dir='+tempFolder,
		'--ignore-certificate-errors',
        '--no-sandbox',
		'--start-maximized'		
	], headless: false, defaultViewPort:null	};
	// making fullscreen from defaultViewPort to null
	// otherwise making a custom resolution here:
	// await page.setViewport({ width: widthWindow, height: heightWindow});
	
	puppeteer = require('puppeteer');
	browser = await puppeteer.launch(ops);
	var context = await browser.createIncognitoBrowserContext();
    page = await context.newPage();

	
	// ensuring the javascript keep running 
	// altough the browse isn't in the main focus (front)
	var session = await page.target().createCDPSession();
	await session.send('Page.enable');
	await session.send('Page.setWebLifecycleState', {state: 'active'});
	
		await page.setViewport({width:0, height:0});

		await setTimeOut(timeWait*2);

		await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');
		//await page.setExtraHTTPHeaders({ DNT: "1" });
		await printout('Setting up the browser is fine!');
		
	} catch(err){
		await printout('some error while setTheBrowser');
		printout(err.toString());
		printout(err.stack);
	}		
}

async function setTimeOut(n){
	
	try{
	await page.setDefaultNavigationTimeout(n);
	await page.setDefaultTimeout(n);
	} catch(err){
		await printout('some error while setTimeOut');
		printout(err.toString());
		printout(err.stack);
	}
	
}

async function visitURL(){
	
	if(indexURLData == 0 || indexURLData < allURLData.length){
		await printout('Trying to visit url ' + urlTarget);
		
		try{
		sleep(timeWait*2.5);
		await setTimeOut(timeWait*2.5);
		await page.goto(urlTarget);
		} catch(err){
		await printout('page.goto() timeout!');	
		await page._client.send("Page.stopLoading");	
		}
		stillVisit = true;
		await sleep(timeWait);
	}else{
		stillVisit = false;
		await printout('waiting for closing any visitors....');
	}
	
	
	
}

async function filterBlocks(){
	var domains_in_blacklist = [
  "www.google-analytics.com",
  "www.googletagmanager.com",
  "www.gstatic.com",
  "www.googleadservices.com",
  "googleads.g.doubleclick.net"
	];
	
	try
	{
	
	await page.setRequestInterception(true);
	page.on('request', (req) => {
		
		var url = req.url();
		var shouldBlock = domains_in_blacklist.includes(new URL(url).host);
		 
		  if(req.resourceType() === 'font'){
			req.abort();
		  }else if(shouldBlock === true){
            req.abort();
        }
        else {
            req.continue();
        }
    });
	
	await printout('Filtering blocks are ready!');
	
	} catch(err){
		await printout('some error while filterBlocks');
		printout(err.toString());
		printout(err.stack);
	}
}

async function addOverall(arr){
	
	for(var i=0; i<arr.length; i++){
		allURLData.push(arr[i]);
	}
	
}

async function login(){

try 

{
	
await printout('trying to logging in...');
await page.hover('#dropdown');
 
 await clickManual('.account-dropdown__login a');

	//await setTimeOut(timeWait*2);
	await sleep(timeWait);
	
	var lokasiUsername = "input[placeholder='Email atau Username']";
	await page.type(lokasiUsername, username);
	
	var lokasiPass = "input[placeholder='Password']";
	await page.type(lokasiPass, pass);
	
	//await setTimeOut(timeWait*2);
	await sleep(timeWait);
	
	
	await clickManual('#loginBtn');

	await printout('logging in success....!');

	//setTimeOut(timeWait*2);
	await sleep(timeWait);
	successLogin = true;
} catch(err){
		await printout('some error while login');
		printout(err.toString());
		printout(err.stack);
		successLogin = false;
}
	
	return successLogin;
	
}



async function clickManual(selector){
	
	// this code used because
	// the normal click is not working properly
	// page.click(selector); -> broken?
	try
	{
	 await page.$eval(selector, elem => elem.click()); 
	} catch(err) {
		await printout('some error while clickManual');
		printout(err.toString());
		printout(err.stack);
	}
	
}

async function extractDetailArticle(){
	
	
	try{
		
	await page.waitForSelector("a[id='seller-mb']", {
        timeout: timeWait
      });
	  
	await page.focus("a[id='seller-mb']");
	//await page.waitForSelector("#seller-mb", {visible: true}); 

	// force to stop the page
	await page._client.send("Page.stopLoading");	

	await printout('clicking phone number...');
	await clickManual("a[id='seller-mb']");
	
	// just a gap to click another element
	await sleep(timeWait-5000);
	try{
	await printout('clicking description opener...');
	await page.waitForSelector("a[class='morelink']", {
        timeout: timeWait-5000
      });
	await clickManual("a[class='morelink']");
	} catch (erx){
		await printout('no, we found no description text. Lets proceed...');
	}
	//setTimeOut(timeWait*2);
	//await sleep(timeWait);
	

	await page.addScriptTag({path: jqueryPath});
	//await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'});
	
	var result = await page.evaluate(() => {
        try {
            var data = '';
				
				//$('#banner-logo').hide();
				var urlPhone = $("#mb-number-popup").find('img').attr('src');
				const nama = $.trim($("h1[class='product-name']").text());
                const loc = $.trim($("div[class='product-location']").text());
                const desc = $.trim($("div[class='product-desc']").text());
                data = {
                    'name' : nama,
                    'location'   : loc,
					'description' : desc,
					'urlPhone' : urlPhone
                };
          
            return data; 
        } catch(err) {
            //reject(err.toString());
			return err.toString();
        }
    });		
	
	await printout('data raw obtained!');
	//await printout('we got raw: \n' +
	//	"==========================\n" +	
	//	JSON.stringify(result) + 
	//	"\n==========================\n");
	
	// translating the phone number
	// from the url image
	// then push into the array once translated
	if(result.urlPhone.includes('gif')){
		// do not download the image, just store empty phone
		await storeEmptyPhone(result);
	}else{
		await download(result, indexURLData);	
	}
	
	//setTimeOut(timeWait*2);
	await sleep(timeWait);
	
	} catch(err){
		
		await printout('some error while extractDetailArticle');
		await printout('probably the page is no longer exist...!');
		//printout(err.toString());
		//printout(err.stack);
		extractDone = true;
		indexURLData++;
		countSummary(false);
	}	
	
}

async function storeEmptyPhone(prevResult){
	
	try
	{
	
	var newResult = {
	  'name' : prevResult.name,
	  'location' : prevResult.location,
	  'description' : prevResult.description,
	  'url' : urlTarget,
	  'phone' : 0
  };
  
  printout('we got only raw: \n' +
		"==========================\n" +	
		JSON.stringify(newResult) + 
		"\n==========================\n");
		
  detailData.push(newResult);
  
  // writing to DB
  writeData(JSON.stringify(detailData));
  
		extractDone = true;
		indexURLData++;
  
  // counting failed data
  countSummary(false);
  
  await printout('a phone is empty, but stored nicely! Total now is ' + detailData.length);
	} catch(err){
		await printout('some error while storeEmptyPhone');
		printout(err.toString());
		printout(err.stack);
		countSummary(false);
	}
}

async function extractArticleURL(){
	
	try
	{
	
	await page.waitForSelector('.product-item', {timeout: timeWait});
	
	await page.addScriptTag({path: jqueryPath});
	//await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'});
	
	var result = await page.evaluate(() => {
        try {
            var data = [];
            $('.product-item').each(function() {
                const url = $(this).find('a').attr('href');
                const title = $(this).find('a').attr('title')
                data.push({
                    'title' : title,
                    'url'   : url
                });
            });
            return data; // Return our data array
        } catch(err) {
            reject(err.toString());
        }
    });
	
	await addOverall(result);
	await printout('Extracting Article URL ' + result.length);
	await printout('Total Overall is ' + allURLData.length);
	
	result = null;
	
	} catch(err){
		
		await printout('some error while extractArticleURL');
		printout(err.toString());
		printout(err.stack);
	}
}

async function isNextPageAvailable(){
	await printout('Checking navigation is available...');
	
	try{
		await page.waitForSelector("a[class='next_page']", {timeout: timeWait});
				stillHasNextPage = true;
	} catch (err){
		stillHasNextPage = false;
		await printout('Navigation is ended!');
		
	}
	
}

async function clickNextPage(){
	try {
	await printout('clicking next page...');
	await clickManual('a[class="next_page"]');
	} catch(err){
		await printout('some error while clickNextPage');
		printout(err.toString());
		printout(err.stack);
	}
}

async function printout(message){
	process.stdout.write(message +'\n');
}

async function printoutR(message){
	process.stdout.write(message +'\r');
}

async function download(result, index){
	// result is a json object 
	
	try{
	
	await printout("trying to donwload image..." + index);
		
	var uri = result.urlPhone;
	
	var filename = databaseFolder + index + '.png';

	//var request = require('request');
	var fetch = require('node-fetch');
	var fs = require('fs');
	
	await fetch(uri)
    .then(res => {
        const dest = fs.createWriteStream(filename);
        res.body.pipe(dest).on('close', function(){
		translateImage(filename, index, result);
		});
    });
	
	//await request.head(uri, function(err, res, body){
    //console.log('content-type:', res.headers['content-type']);
    //console.log('content-length:', res.headers['content-length']);
	
    //request(uri).pipe(fs.createWriteStream(filename)).on('close', function(){
	//	translateImage(filename, index, result);
	//});
	
	//});
  
	} catch(err){
		
		await printout('some error while download');
		//printout(err.toString());
		//printout(err.stack);
		
		// if let say the image itself unreachaable 
		// in some other case, we still store empty phone
		await storeEmptyPhone(result);
		
	}
	
	
};

async function translateImage(filename, index, prevResult){
	
	// prevResult is json object
	try
	{

var Path = require('path');

var alamatLang = Path.resolve(__dirname, 'tesseract_lang');

var fs = require('fs');

const { createWorker } = require('tesseract.js');
const worker = createWorker({
  langPath: alamatLang
});

 await worker.load();
  await worker.loadLanguage('eng');
  await worker.initialize('eng');

await worker.recognize(filename, 'eng').then(({ data: { text } }) => {
  text = text.replace('_','').trim();
  printout('translated phone ' + text);
  
  // deleting the image data 
  fs.unlinkSync(filename);
  
  var newResult = {
	  'name' : prevResult.name,
	  'location' : prevResult.location,
	  'description' : prevResult.description,
	  'url' : urlTarget,
	  'phone' : text,
	  'tanggal' : '-'
  };
  
   printout('we got raw: \n' +
		"==========================\n" +	
		JSON.stringify(newResult) + 
		"\n==========================\n");
  
  detailData.push(newResult);
  
   //writing to the DB
	//writeData(JSON.stringify(newResult));
	writeData(JSON.stringify(detailData));
  
  printout('didapat data translated total ' + detailData.length);
  extractDone = true;
  indexURLData++;
  
  // deleting the english trainee data
  var alamatTrainned = Path.resolve(__dirname, 'eng.traineddata');
  fs.unlinkSync(alamatTrainned);
  
  // counting success data
  countSummary(true);
  
});

await worker.terminate();
	} catch(err){
		await printout('some error while translateImage');
		//printout(err.toString());
		//printout(err.stack);
		 printout('we got only previous raw: \n' +
		"==========================\n" +	
		JSON.stringify(prevResult) + 
		"\n==========================\n");
		
		extractDone = true;
		countSummary(false);
	}
//await sleep(5000);
	
}

function clearAllTimer(){
	
	for(var i=0; i<dataTimerID.length; i++){
		var nom = dataTimerID[i];
		clearInterval(nom);
	}
	
	dataTimerID = [];
}

function countSummary(successStat){
	
	totalData = allURLData.length;
	
	if(successStat == true){
		successData++;
	} else{
		failedData++;
	}
	
}

function timer(time,update,complete) {
    var start = new Date().getTime();
    var interval = setInterval(function() {
        var now = time-(new Date().getTime()-start);
        if( now <= 0) {
            clearInterval(interval);
            complete();
        }
        else update(Math.floor(now/1000));
    },1000); 
	
	dataTimerID.push(interval);
}

async function sleep(ms) {
	try{
  //return new Promise((resolve) => {
   // setTimeout(resolve, ms);
  //});
    // clear first any interval working
	clearAllTimer();
  
	var timena = ms/1000;
	timena--;
  
	timer(
		ms, // milliseconds
    function(timeleft) { // called every step to update the visible countdown
        printoutR("timer..."+timena);
		timena--;
    },
    function() { // what to do after
        printout('processing...');
		if(stillVisit==false){
			// close the process
				printout('happy scrapping!');
				printout('==================================');
				printout('Total Data :' + totalData);
				printout('Missing Phone Detail :' + failedData);
				printout('Complete Phone Detail :' + successData);
				printout('==================================');
				process.exit(22);
		}
    }
	);
  
  
  await printout('sleeping for ... ' + (ms/1000) + 's' );
  await page.waitFor(ms);
  
	} catch(err){
		await printout('some error while sleeping');
		printout(err.toString());
		printout(err.stack);	
	}
}

async function countDown(timeIn){
	var time = (timeIn/1000);
	time--;
	if(time>0){
		setTimeout(printoutR('waiting '+time), 1000);
	}
}   

async function writeData(data){
	// the data comes in already in the json stringified format
	try{
		
		var fs = require('fs');
		
		fs.writeFileSync(lokasiFinalDB, data, 'utf8');
		
	await printout('updating data done!');
	
	
	} catch(err){
		
		await printout('some error while writeData');
		printout(err.toString());
		printout(err.stack);
		
	}
}

async function deleteFileOnlyInDir(locationPath){
	
	await deleteDir(locationPath, 'png');
	
}

async function deleteDir(locationPath, extension='default'){
	var fs = require('fs');	
	var PathHelper = require('path');
	
  if (fs.existsSync(locationPath)) {
    fs.readdirSync(locationPath).forEach((file, index) => {
      const curPath = PathHelper.join(locationPath, file);
      if (fs.lstatSync(curPath).isDirectory()) { 
	  
	  // recurse
        deleteDir(curPath);
      } else { 
	  // delete file directly
	  if(extension == 'default'){
		 fs.unlinkSync(curPath);  
	  }else{
		  // delete specific extension file only
		  var extNya = PathHelper.extname(curPath);
		  if(extNya.includes(extension)){
				fs.unlinkSync(curPath);  
		  }
	  }
       
      }
    });
	
	// finally delete the whole folder if there is no one mentioning
	// any extension needed
	if(extension=='default'){
		fs.rmdirSync(locationPath);
	}
  }

}

function captureCTRLC(){
	
	process.stdin.resume();
	
	if (process.platform === "win32") {
  var rl = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
  });

  rl.on("SIGINT", function () {
    process.emit("SIGINT");
  });
}

process.on("SIGINT", function () {
  //graceful shutdown
	printout('we are exitting...');
	process.exit(33);
  
});
	
}