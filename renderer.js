//
// (C) FGroupIndonesia.com team.
// proudly present the Render.js used as a part of visualizer.js
// ===========================================
// first-release-date: 14/sept/2020
// category: internet-marketing, data-scrapper
// ===========================================
// license: Apache License 2.0 and BSD.
// ===========================================
// feel free to make 'pull request' for enhancing the features available from this nodejs app.
// 

var manyTelkomsel 	= 0;
var manyXL 			= 0;
var manyAxis 		= 0;
var manyIndosat 	= 0;
var manyThree 		= 0;
var manySmartfren 	= 0;
var manyNonListed 	= 0;

var indexData		= 0;
var manyFile 		= 0;
var manyDataFound 	= 0;

var dataALL			= [];

var mainDBFolder = "./ex-database/";
/* berisi json format
{
	filename	: 'name.db',
	completed	: true,
	content		: '[{json}]'
}
*/

var dataTempStat 	= [];

var table			= null;
const writer 		= require('fs');
const reader 		= require('fs');
const path 			= require('path');

var duplicates		= false;



$(document).ready(function(){
	
	// #01: first call to :
	// read the file and then 
	// rendering into the table
	play();
	
	$(document).on('click','button', function(){
		// generate Time
		var stat = $(this).attr('aksi');
		var nomer = $(this).attr('phone');
		var namaF = $(this).attr('file');
		saveStatus(nomer, stat, namaF);
		
	});
	
	
});



function play(){
	
	
	countFile(mainDBFolder, ".db");
	
	//#02: everything is here
	// start after 2 seconds
	setTimeout(function(){ 
		startWorking();
	}, 2000);
	
}

function startWorking(){
	
	//#03: everything is here
	// read each file content
	for(var i=0; i<dataALL.length; i++){
		var namaNya = dataALL[i];
		// save the index as well for updating the content reference
			renderData(namaNya.filename, i);
	}
	
}

function countFile(startPath, filter){
	
	
   writer.readdir(startPath, (err, files) => {
	  files.forEach(file => {
		
		if(file.includes(filter)){
			manyFile++;
			//console.log("found " + file);
			var ketFile = {
				'filename' : file,
				'completed' : false,
				'content' : ''
			};
			dataALL.push(ketFile);
		}
		
	  });
	});
	
}

function refreshStatus(){
	
	
	for(i=0; i<dataTempStat.length; i++){
		var statNya = dataTempStat[i].status;
		var nomer 	= dataTempStat[i].phone;
		var filena 	= dataTempStat[i].filename;
		
		var selektor = "button[phone='"+nomer+"']";
		
		$(selektor).each(function(i, obj) {
			var aksiNya = $(this).attr('aksi');
			if(aksiNya==statNya){
				$(this).attr('class','active');
			}
		});
	}
	
	
	
}

function saveStatus(phone, stat, file){
	// changing status of the content specifically inside that file
	// the matching point is the phone
	dataTempStat.push({
		'phone' : phone,
		'status' : stat,
		'filename' : file
	});
	
	var today = new Date();
	var dd = today.getDate();

	var mm = today.getMonth()+1; 
	var yyyy = today.getFullYear();
	if(dd<10) 
	{
		dd='0'+dd;
	} 

	if(mm<10) 
	{
		mm='0'+mm;
	} 
	
	today = mm+'-'+dd+'-'+yyyy;
	var found = false;
	var no = 0;
	
	for(var z=0; z<dataALL.length; z++){
		
		// which file is that?
		if(dataALL[z].filename == file){
			no = z;
			found = true;
			
			var jsObject = dataALL[z].content;
			for(var x=0; x<jsObject.length; x++){
				if(jsObject[x].phone == phone){
					jsObject[x].tanggal = today;
					jsObject[x].status = stat;
					
					// referencing back
					dataALL[z].content = jsObject;
					break;
				}
				
			}				
			
			break;
		}
		
	}
	
	if(found){
	var lokasiBaru = mainDBFolder + file;

    deleteFile(lokasiBaru, no);
	
	var loggerDetail 		= writer.createWriteStream(lokasiBaru, {
	  flags: 'a' 
	});	
		
	var isi = JSON.stringify(dataALL[no].content);
	loggerDetail.write(isi);
	loggerDetail.end();
	
	refreshStatus();
	
	} else {
		console.log('not found for saving state!');
	}
	
}

function deleteFile(lok, nomer){
	writer.unlink(lok, (err) => {
        if (err) {
            alert("An error ocurred updating the file" + err.message);
            console.log(err);
            return;
        }
        console.log("File ["+nomer+"] succesfully deleted");
    });
}

function setTotalOperators(){
	
	$('#xl').text(manyXL);
	$('#axis').text(manyAxis);
	$('#telkomsel').text(manyTelkomsel);
	$('#indosat').text(manyIndosat);
	$('#three').text(manyThree);
	$('#smartfren').text(manySmartfren);
	$('#non-listed').text(manyNonListed);
	
}

function duplicatePhoneRemoval(arrayIn){
	
	// removing duplicate phones entry
	var arrayNew = [];
	
	for(var n=0; n<arrayIn.length; n++){
		
		if(isPhoneIn(arrayIn[n].phone, arrayNew) != true){
			arrayNew.push(arrayIn[n]);
		}
		
	}
	
	console.log('Removing duplicates from total ' + arrayIn.length + ' entries, now sorted into ' + arrayNew.length);
	manyDataFound += arrayNew.length;
	return arrayNew;
}

function isPhoneIn(number, arrayChecked){
	
	for(var x=0; x<arrayChecked.length; x++){
		
		if(arrayChecked[x].phone == number){
			return true;
		}
		
	}
	
	return false;
	
}

 function renderData(fileNameNa, index){
	
	var lokasiFile = mainDBFolder + fileNameNa;
	var konten = "";
	
	reader.readFileSync(lokasiFile, 'utf-8').split(/\r?\n/).forEach(function(line){
		konten = line.trim().replace(/\\n/g, '');
		
		// send to table
			// and also to the array as reference
			// the content would be a json parsed data
			// as array
			//console.log(' ada data ' + konten);
			var dataBaru = JSON.parse(konten);
			
			// removing the duplicates
			var dataKonten = duplicatePhoneRemoval(dataBaru);
			// saved the referenced as well
			dataALL[index].content = dataKonten;
			dataALL[index].completed = true;
			
			renderDataTable(dataKonten, fileNameNa);
		
	});
	
	
}

function isItFrom(array, nomerTelp){
	
	for(i=0; i<array.length; i++){
		if(nomerTelp.includes(array[i])){
			return true;
		}
	}
	
	
	return false;
}

function getOperator(numberPhone){
	var telkomsel = ["0811", "0812", "0813", "0821", "0822", "0852", "0853", "0823", "0851"];
	var indosat = ["0814", "0815", "0816", "0855", "0856", "0857", "0858"];
	var xl = ["0817", "0818", "0819", "0859", "0877", "0878"];
	var axis = ["0838", "0831", "0832", "0833"];
	var three = ["0895", "0896", "0897", "0898", "0899"];
	var smartfren = ["0881", "0882", "0883", "0884", "0885", "0886", "0887", "0888", "0889"];
	
	numberPhone = numberPhone.toString();
	
	if (numberPhone == 'undefined'){
		manyNonListed++;
		return "non-listed";
	} else if(isItFrom(telkomsel, numberPhone)){
		manyTelkomsel++;
		return "telkomsel";
	} else if(isItFrom(indosat, numberPhone)){
		manyIndosat++;
		return "indosat";
	} else if(isItFrom(xl, numberPhone)){
		manyXL++;
		return "xl";
	} else if(isItFrom(axis, numberPhone)){
		manyAxis++;
		return "axis";
	} else if(isItFrom(three, numberPhone)){
		manyThree++;
		return "three";
	} else if(isItFrom(smartfren, numberPhone)){
		manySmartfren++;
		return "smartfren";
	}else{
		manyNonListed++;
		return "non-listed";
	}
		
	
	
	return null;
}



function isAllDataComplete(){
	var komplitSemua = false;
	
	// console.log('ada data ' + manyDataFound);
	// console.log('ada data ke ' + indexData);
	if(indexData == manyDataFound){
		komplitSemua = true
	}
	
	return komplitSemua;
	
}



function renderDataTable(arrayIn, namaNya){
	
	//console.log('rendering ... ' + stringContent);
	// one single array with big data inside
	for(var x=0; x<arrayIn.length; x++){
	
	var jsObject = arrayIn[x];
	
	var namaAcuan = namaNya;	
		//console.log('rendering ... ' + stringContent);
	var data = jsObject;
	
	var tanggalModif = data.tanggal;
	var stat = data.status;
	
	var buttonStatSuccess = "";
	var buttonStatHangup = "";
	var buttonStatInvalid = "";
	var buttonStatPending = "";
	
	// check whether this data has tanggal property
	// and get its status as well
	if(tanggalModif != "-"){
		
		if(stat=='success'){
			buttonStatSuccess = "active";
		}else if(stat=='hangup'){
			buttonStatHangup = "active";
		}else if(stat=='invalid'){
			buttonStatInvalid = "active";
		}else if(stat=='pending'){
			buttonStatPending = "active";
		}
		
	}
	
	var index = x;
	var newElement = "<tr>" +
		"<td>" +(index+1) + "</td>"+
		"<td>" +data.name + "</td>"+
		"<td>" +data.description + "</td>"+
		"<td>" +data.location + "</td>"+
		"<td>" +data.phone + "</td>"+
		"<td>" +getOperator(data.phone) + "</td>"+
		"<td>" +tanggalModif+ "</td>"+
		"<td> <button file='"+namaAcuan+"' class='"+buttonStatSuccess+"' aksi='success' phone='"+data.phone+"'>Success</button>"+
		"<button file='"+namaAcuan+"' class='"+buttonStatPending+"' aksi='pending' phone='"+data.phone+"'>Pending</button>"+
		"<button file='"+namaAcuan+"' class='"+buttonStatHangup+"' aksi='hangup' phone='"+data.phone+"'>Hangup</button>"+
		"<button file='"+namaAcuan+"' class='"+buttonStatInvalid+"' aksi='invalid' phone='"+data.phone+"'>Invalid</button>"+
		" </td>"+
	"</tr>";
	
	indexData++;
	
	$('#main-table tbody').append(newElement);
	
		if(isAllDataComplete() == true){
			table = $('#main-table').DataTable( {
				"destroy" : true,
				"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ]
			} );
			setTotalOperators();
			$('#loading').hide();
		}
	
	}
	
}