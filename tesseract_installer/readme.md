
# Tesseract-OCR Notice

1. Install the *tesseract-ocr-setup-3.02.02.exe* as usual.
2. Copy *eng.traineddata* to the root directory.
3. close any commandprompt. And you may delete this folder afterwards.
4. Run the script as mentioned in the root directory instruction.

Thanks for everything!

*happy scrapping!*

**(c) FGroupIndonesia team, 2020.**

